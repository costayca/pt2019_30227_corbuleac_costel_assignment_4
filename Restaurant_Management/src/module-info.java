module Restaurant.Management {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.base;
    requires javafx.graphics;

    opens main;
    opens business;
    opens presentation;
    opens data;
}