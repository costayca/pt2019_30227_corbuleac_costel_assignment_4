package data;

import business.CompositeProduct;
import business.MenuItem;
import presentation.MenuDisplay;
import main.Main;

import java.io.*;
import java.util.ArrayList;
import java.util.Set;

public class RestaurantSerializator implements Serializable {
    public static void serialization(Set<MenuItem> menuItems) {
        try {
            String filename = "./resources/menu/restaurant.ser";
            FileOutputStream file = new FileOutputStream(filename);
            ObjectOutputStream out = new ObjectOutputStream(file);

            out.writeInt(menuItems.size());
            System.out.println(menuItems.size());

            for (MenuItem item : menuItems) {
                System.out.println(item);
                ArrayList<Integer> items = new ArrayList<>();
                int sum = 0;
                if (item instanceof CompositeProduct) {
                    for (MenuItem secItem : ((CompositeProduct) item).getComponentItems()) {
                        items.add(secItem.getId());
                        sum += secItem.computePrice();
                    }
                }
                out.writeObject(new MenuDisplay(item.getId(), item.getName(), item.computePrice() - sum, items));

            }

            out.close();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void deserialization(Set<MenuItem> menuItems) {
        try {
            String filename = "./resources/menu/restaurant.ser";
            FileInputStream file = new FileInputStream(filename);
            ObjectInputStream in = new ObjectInputStream(file);

            int i;
            try {
                i = in.readInt();
            } catch (IOException e) {
                i = 0;
            }
            System.out.println(i);
            MenuDisplay menu;
            for (int j = 0; j < i; j++) {
                menu = (MenuDisplay) in.readObject();
                System.out.println(menu);
                Main.restaurant.addItem(menu.getId(), menu.getName(), menu.getPrice(), menu.getCompositeID());
            }

            in.close();
            file.close();

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
