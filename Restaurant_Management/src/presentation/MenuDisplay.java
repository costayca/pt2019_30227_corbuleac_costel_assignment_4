package presentation;

import java.io.Serializable;
import java.util.ArrayList;

public class MenuDisplay implements Serializable {
    public Integer id;
    public String name;
    public Integer price;
    public ArrayList<Integer> compositeID;

    public MenuDisplay(int id, String name, int price, ArrayList<Integer> compositeID) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.compositeID = compositeID;
    }

    @Override
    public String toString() {
        return "MenuDisplay{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", compositeID=" + compositeID +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ArrayList<Integer> getCompositeID() {
        return compositeID;
    }

    public void setCompositeID(ArrayList<Integer> compositeID) {
        this.compositeID = compositeID;
    }
}
