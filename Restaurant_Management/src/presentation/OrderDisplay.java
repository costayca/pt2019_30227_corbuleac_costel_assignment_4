package presentation;

import java.util.ArrayList;
import java.util.Date;

public class OrderDisplay {
    public Integer id;
    public Date date;
    public Integer table;
    public Integer price;
    public ArrayList<Integer> compositeID;

    public OrderDisplay(Integer id, Date date, Integer table, Integer price, ArrayList<Integer> compositeID) {
        this.id = id;
        this.date = date;
        this.table = table;
        this.price = price;
        this.compositeID = compositeID;
    }

    @Override
    public String toString() {
        return "OrderDisplay{" +
                "id=" + id +
                ", date=" + date +
                ", table=" + table +
                ", price=" + price +
                ", compositeID=" + compositeID +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Integer getTable() {
        return table;
    }

    public void setTable(Integer table) {
        this.table = table;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public ArrayList<Integer> getCompositeID() {
        return compositeID;
    }

    public void setCompositeID(ArrayList<Integer> compositeID) {
        this.compositeID = compositeID;
    }
}
