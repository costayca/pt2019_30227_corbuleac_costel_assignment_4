package main;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

public class WaiterControl {

    @FXML
    public HBox insertPlace;

    private HBox addHBox(String s, String s2) {
        HBox newH = new HBox();
        newH.getChildren().add(new Label(s));
        TextField newT = new TextField();
        newT.setPromptText(s2);
        newH.getChildren().add(newT);
        return newH;
    }

    public void handleAdd() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Order ID:", "Introduce ID");
        HBox newH1 = addHBox("Order Table:", "Introduce table number");
        HBox newH2 = addHBox("Menu Items:", "Introduce menu ID's");
        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Submit Data");
        B1.setOnAction(e -> {
            handleAddOrder();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH1);
        toInsert.getChildren().add(newH2);
        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleAddOrder() {
        OrderD orderD = new OrderD().invoke();
        int id = orderD.getId();
        int table = orderD.getTable();
        ArrayList<Integer> ids = orderD.getIds();

        Main.restaurant.addOrder(id, table, ids);
    }

    public void handleBill() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Order ID:", "Introduce ID");
        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Create Bill");
        B1.setOnAction(e -> {
            handleGenerateBill();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);

    }

    private void handleGenerateBill() {
        ArrayList<TextField> fields = getTextFields(1);
        int id = Integer.parseInt(fields.get(0).getText());
        try {
            FileWriter writer = new FileWriter("Order" + id + ".txt");
            writer.write(Main.restaurant.generateBill(id));
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void handleDetails() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/orderTable.fxml"));
        Stage newStage = new Stage();
        Parent root1 = null;
        try {
            root1 = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        root1.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage.setScene(new Scene(root1, 800, 600));
        newStage.setTitle("Order Table");
        newStage.show();
    }

    public void computePrice() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Order ID:", "Introduce ID");
        HBox newH1 = addHBox("Order Price:", "Price will be shown here");
        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Show Price");
        B1.setOnAction(e -> {
            handleSetPrice();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH1);
        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleSetPrice() {
        ArrayList<TextField> fields = getTextFields(2);
        int id = Integer.parseInt(fields.get(0).getText());
        fields.get(1).setText(Main.restaurant.calculatePrice(id) + "");
    }

    private ArrayList<TextField> getTextFields(int nr) {
        ArrayList<TextField> fields = new ArrayList<>();
        for (int i = 0; i < nr; i++) {
            fields.add((TextField) (((HBox) ((VBox) insertPlace.getChildren().get(0)).getChildren().get(i)).getChildren().get(1)));
        }
        return fields;
    }

    private class OrderD {
        private int id;
        private int table;
        private ArrayList<Integer> ids;

        public int getId() {
            return id;
        }

        public int getTable() {
            return table;
        }

        public ArrayList<Integer> getIds() {
            return ids;
        }

        public OrderD invoke() {
            ArrayList<TextField> fields = getTextFields(3);
            id = Integer.parseInt(fields.get(0).getText());
            table = Integer.parseInt(fields.get(1).getText());
            String[] sIds = fields.get(2).getText().split(" ");
            ids = new ArrayList<>();
            for (String s : sIds) {
                if (s.length() > 0)
                    ids.add(Integer.parseInt(s));
            }
            return this;
        }
    }
}
