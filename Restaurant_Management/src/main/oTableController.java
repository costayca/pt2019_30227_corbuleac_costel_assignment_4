package main;

import business.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import presentation.OrderDisplay;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

public class oTableController {
    @FXML
    private VBox container;

    @FXML
    public void initialize() {
        TableView<OrderDisplay> table = new TableView<>();


        TableColumn<OrderDisplay, Integer> idColumn = new TableColumn<>("ID");
        idColumn.setMinWidth(150);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<OrderDisplay, Date> dateColumn = new TableColumn<>("Date");
        dateColumn.setMinWidth(150);
        dateColumn.setCellValueFactory(new PropertyValueFactory<>("date"));

        TableColumn<OrderDisplay, Integer> tableColumn = new TableColumn<>("Table");
        tableColumn.setMinWidth(150);
        tableColumn.setCellValueFactory(new PropertyValueFactory<>("table"));

        TableColumn<OrderDisplay, Integer> priceColumn = new TableColumn<>("Price");
        priceColumn.setMinWidth(150);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn<OrderDisplay, ArrayList<Integer>> compositeIdColumn = new TableColumn<>("Ordered Items");
        compositeIdColumn.setMinWidth(150);
        compositeIdColumn.setCellValueFactory(new PropertyValueFactory<>("compositeID"));

        table.getColumns().add(idColumn);
        table.getColumns().add(dateColumn);
        table.getColumns().add(tableColumn);
        table.getColumns().add(priceColumn);
        table.getColumns().add(compositeIdColumn);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        table.setItems(getOrder());
        container.getChildren().add(table);
    }

    public ObservableList<OrderDisplay> getOrder() {
        ObservableList<OrderDisplay> log = FXCollections.observableArrayList();
        for (Map.Entry<Order, ArrayList<MenuItem>> entry : Main.restaurant.getOrderLog().entrySet()) {
            Order key = entry.getKey();
            ArrayList<MenuItem> value = entry.getValue();
            int sum = 0;
            ArrayList<Integer> menuId = new ArrayList<>();
            for (MenuItem menu: value) {
                sum += menu.computePrice();
                menuId.add(menu.getId());
            }
            log.add(new OrderDisplay(key.getOrderId(),key.getDate(),key.getTable(),sum,menuId));
        }
        for (OrderDisplay item : log) {
            System.out.println(item);
        }
        return log;
    }
}
