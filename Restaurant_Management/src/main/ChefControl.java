package main;

import presentation.OrderDisplay;
import javafx.scene.control.TextArea;

import java.util.Observable;
import java.util.Observer;

public class ChefControl implements Observer {

    public TextArea chef;


    @Override
    public void update(Observable o, Object arg) {
        OrderDisplay newOrder = (OrderDisplay) arg;

        chef.setText(chef.getText() + String.format("Time: %s\nPreparing order number: %d\n", newOrder.getDate(), newOrder.getId()));
        System.out.println(String.format("Time: %s\nPreparing order number: %d\n", newOrder.getDate(), newOrder.getId()));
        for (Integer menu : newOrder.getCompositeID()) {
            chef.setText(chef.getText() + String.format("Cooking menu number: %d\n", menu));
            System.out.println(String.format("Cooking menu number: %d\n", menu));
        }

        chef.setText(chef.getText() + String.format("Serving meal to table number: %d\n\n", newOrder.getTable()));
        System.out.println(String.format("Serving meal to table number: %d\n\n", newOrder.getTable()));
    }
}
