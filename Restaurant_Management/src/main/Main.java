package main;

import business.MenuItem;
import business.Restaurant;
import business.RestaurantProcessing;
import data.RestaurantSerializator;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Main extends Application {

    public static RestaurantProcessing restaurant;

    @Override
    public void start(Stage primaryStage) throws Exception {

        restaurant = new Restaurant();

        RestaurantSerializator.deserialization(restaurant.getRestaurantMenu());

//        restaurant.addItem(1,"Cartof",2,new ArrayList<>());
//        restaurant.addItem(2,"Hamburger",2,new ArrayList<>());
//        ArrayList<Integer> add = new ArrayList<>();
//        add.add(1);
//        add.add(2);
//        restaurant.addItem(3,"1+1=5",1,add);
//        add.remove(0);
//        add.remove(0);
//        restaurant.addItem(4,"Piftea",4,new ArrayList<>());

//        add.add(1);
//        restaurant.addOrder(1,2,add);
//        add.remove(0);
//        add.add(1);
//        add.add(2);
//        restaurant.addOrder(2,1,add);
//        add.remove(0);
//        add.remove(0);
//        add.add(1);
//        add.add(2);
//        add.add(3);
//        add.add(4);
//        restaurant.addOrder(3,3,add);

        for (MenuItem item : restaurant.getRestaurantMenu()) {
            System.out.println(item);
        }

        Font.loadFont(getClass().getResource("/fonts/VarelaRound-Regular.ttf").toExternalForm(), 10);
        Parent root = FXMLLoader.load(getClass().getResource("/fxml/administrator.fxml"));
        root.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        primaryStage.setTitle("Administrator Window");
        primaryStage.setScene(new Scene(root, 800, 600));
        primaryStage.show();

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/waiter.fxml"));
        Stage newStage = new Stage();
        Parent root1 = loader.load();
        root1.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage.setScene(new Scene(root1, 800, 600));
        newStage.setTitle("Waiter Window");
        newStage.show();

        FXMLLoader loader2 = new FXMLLoader(getClass().getResource("/fxml/chef.fxml"));
        Stage newStage2 = new Stage();
        Parent root2 = loader2.load();
        root2.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        ChefControl controller = loader2.getController();
        newStage2.setScene(new Scene(root2, 800, 600));
        newStage2.setTitle("Chef Window");
        newStage2.show();

        ((Restaurant) restaurant).addObserver(controller);
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        RestaurantSerializator.serialization(restaurant.getRestaurantMenu());
    }

    public static void main(String[] args) {
        launch(args);
    }
}
