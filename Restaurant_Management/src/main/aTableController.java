package main;

import business.CompositeProduct;
import presentation.MenuDisplay;
import business.MenuItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

import java.util.ArrayList;

public class aTableController {
    @FXML
    private VBox container;

    @FXML
    public void initialize() {
        TableView<MenuDisplay> table = new TableView<>();


        TableColumn<MenuDisplay, Integer> idColumn = new TableColumn<>("ID");
        idColumn.setMinWidth(180);
        idColumn.setCellValueFactory(new PropertyValueFactory<>("id"));

        TableColumn<MenuDisplay, String> nameColumn = new TableColumn<>("Name");
        nameColumn.setMinWidth(180);
        nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<MenuDisplay, Integer> priceColumn = new TableColumn<>("Price");
        priceColumn.setMinWidth(180);
        priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

        TableColumn<MenuDisplay, ArrayList<Integer>> compositeIdColumn = new TableColumn<>("Component id's");
        compositeIdColumn.setMinWidth(200);
        compositeIdColumn.setCellValueFactory(new PropertyValueFactory<>("compositeID"));

        table.getColumns().add(idColumn);
        table.getColumns().add(nameColumn);
        table.getColumns().add(priceColumn);
        table.getColumns().add(compositeIdColumn);

        table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

        table.setItems(getMenu());
        container.getChildren().add(table);
    }

    public ObservableList<MenuDisplay> getMenu() {
        ObservableList<MenuDisplay> menu = FXCollections.observableArrayList();
        for (MenuItem item : Main.restaurant.getRestaurantMenu()) {
            ArrayList<Integer> menuId = new ArrayList<>();
            if (item instanceof CompositeProduct) {
                for (MenuItem cItem : ((CompositeProduct) item).getComponentItems()) {
                    menuId.add(cItem.getId());
                }
            }
            menu.add(new MenuDisplay(item.getId(), item.getName(), item.computePrice(), menuId));
        }
        for (MenuDisplay item : menu) {
            System.out.println(item);
        }

        return menu;
    }
}
