package main;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.ArrayList;

public class AdministratorControl {
    public HBox insertPlace;

    public void handleAddList() {
        System.out.println("Been Here");
        MenuD menuD = new MenuD().invoke();
        int id = menuD.getId();
        int price = menuD.getPrice();
        String name = menuD.getName();
        ArrayList<Integer> ids = menuD.getIds();
        Main.restaurant.addItem(id, name, price, ids);
    }

    private ArrayList<TextField> getTextFields(int nr) {
        ArrayList<TextField> fields = new ArrayList<>();
        for (int i = 0; i < nr; i++) {
            fields.add((TextField) (((HBox) ((VBox) insertPlace.getChildren().get(0)).getChildren().get(i)).getChildren().get(1)));
        }
        return fields;
    }

    public void handleEditList() {
        System.out.println("Done That");
        MenuD menuD = new MenuD().invoke();
        int id = menuD.getId();
        int price = menuD.getPrice();
        String name = menuD.getName();
        ArrayList<Integer> ids = menuD.getIds();
        System.out.println(Main.restaurant.editItem(id, name, price, ids));
    }

    public VBox addFields() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Menu ID:", "Introduce ID");

        HBox newH1 = addHBox("Menu Name:", "Introduce Name");

        HBox newH2 = addHBox("Menu Price:", "Introduce Price");

        HBox newH3 = addHBox("Composite ID's:", "Introduce ID's of the member products");

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH1);
        toInsert.getChildren().add(newH2);
        toInsert.getChildren().add(newH3);

        return toInsert;
    }

    private HBox addHBox(String s, String s2) {
        HBox newH = new HBox();
        newH.getChildren().add(new Label(s));
        TextField newT = new TextField();
        newT.setPromptText(s2);
        newH.getChildren().add(newT);
        return newH;
    }

    public void handleAdd() {

        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Submit Data");
        B1.setOnAction(e -> {
            handleAddList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    public void handleEdit() {

        VBox toInsert = addFields();

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Edit Data");
        B1.setOnAction(e -> {
            handleEditList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    public void handleDelete() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        VBox toInsert = new VBox();
        toInsert.getStyleClass().add("input-container");

        HBox newH = addHBox("Menu ID:", "Introduce ID");

        HBox newH4 = new HBox();
        Button B1 = new Button();
        B1.setText("Delete Data");
        B1.setOnAction(e -> {
            handleDeleteList();
        });
        newH4.getChildren().add(B1);

        toInsert.getChildren().add(newH);
        toInsert.getChildren().add(newH4);
        insertPlace.getChildren().add(toInsert);
    }

    private void handleDeleteList()         {
        System.out.println("Delete this");
        ArrayList<TextField> fields = getTextFields(1);
        int id = Integer.parseInt(fields.get(0).getText());
        Main.restaurant.deleteItem(id);
    }

    public void handleView() {
        if (insertPlace.getChildren().size() > 0)
            insertPlace.getChildren().remove(0);

        FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/adminTable.fxml"));
        Stage newStage = new Stage();
        Parent root1 = null;
        try {
            root1 = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        root1.getStylesheets().add(getClass().getResource("/css/main.css").toExternalForm());
        newStage.setScene(new Scene(root1, 800, 600));
        newStage.setTitle("Menu Table");
        newStage.show();
    }

    private class MenuD {
        private int id;
        private int price;
        private String name;
        private ArrayList<Integer> ids;

        public int getId() {
            return id;
        }

        public int getPrice() {
            return price;
        }

        public String getName() {
            return name;
        }

        public ArrayList<Integer> getIds() {
            return ids;
        }

        public MenuD invoke() {
            ArrayList<TextField> fields = getTextFields(4);
            id = Integer.parseInt(fields.get(0).getText());
            price = Integer.parseInt(fields.get(2).getText());
            name = fields.get(1).getText();
            String[] sIds = fields.get(3).getText().split(" ");
            ids = new ArrayList<>();

            for (String s : sIds) {
                if (s.length() > 0)
                    ids.add(Integer.parseInt(s));
            }
            return this;
        }
    }
}
