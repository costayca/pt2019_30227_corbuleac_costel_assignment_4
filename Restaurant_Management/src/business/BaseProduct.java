package business;

import java.io.Serializable;

public class BaseProduct extends MenuItem {

    private int basePrice;

    public BaseProduct(int id, int basePrice, String name) {
        setId(id);
        this.basePrice = basePrice;
        setName(name);
    }

    public void setBasePrice(int basePrice) {
        this.basePrice = basePrice;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int computePrice() {
        return this.basePrice;
    }

    @Override
    public String toString() {
        return String.format("BaseProduct:\nmenuId= %d\nname= %s\nprice= %d\n", getId(), getName(), basePrice);
    }
}
