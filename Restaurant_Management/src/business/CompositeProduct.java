package business;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompositeProduct extends MenuItem {


    private int compositePrice;

    private List<MenuItem> componentItems;

    public CompositeProduct(int id, String name) {
        setId(id);
        setName(name);
        this.componentItems = new ArrayList<>();
        this.compositePrice = 0;
    }

    public void addComponentItems(MenuItem componentItems) {
        this.componentItems.add(componentItems);
        this.compositePrice += componentItems.computePrice();
    }

    public void removeComponentItems(MenuItem componentItems) {
        this.componentItems.add(componentItems);
        this.compositePrice -= componentItems.computePrice();
    }

    public void increasePrice(int price) {
        this.compositePrice += price;
    }

    public void setCompositePrice(int compositePrice) {
        this.compositePrice = compositePrice;
    }

    public void setComponentItems(List<MenuItem> componentItems) {
        this.componentItems = componentItems;
    }

    public List<MenuItem> getComponentItems() {
        return componentItems;
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public void setName(String name) {
        super.setName(name);
    }

    @Override
    public int computePrice() {
        return this.compositePrice;
    }

    @Override
    public String toString() {
        return String.format("BaseProduct:\nmenuId= %d\nname= %s\nprice= %d\ncomponent Items= %s\n", getId(), getName(), compositePrice, componentItems);
    }
}
