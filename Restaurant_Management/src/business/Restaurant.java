package business;

import presentation.OrderDisplay;

import java.util.*;

public class Restaurant extends Observable implements RestaurantProcessing {
    private HashMap<Order, ArrayList<MenuItem>> orderLog;
    private Set<MenuItem> restaurantMenu;

    public Restaurant() {
        this.orderLog = new HashMap<>();
        this.restaurantMenu = new TreeSet<>(new IntegerComparator());
    }

    public HashMap<Order, ArrayList<MenuItem>> getOrderLog() {
        return orderLog;
    }

    public Set<MenuItem> getRestaurantMenu() {
        return restaurantMenu;
    }

    public Set<Order> getOrders() {
        return orderLog.keySet();
    }

    public boolean searchOrder(int orderId) {
        for (Order order : orderLog.keySet()) {
            if (order.getOrderId() == orderId) {
                return true;
            }
        }
        return false;
    }

    public MenuItem searchMenuId(int menuId) {
        for (MenuItem menu : restaurantMenu) {
            if (menu.getId() == menuId)
                return menu;
        }
        return null;
    }

    public int addOrder(int orderId, int orderTable, ArrayList<Integer> menuItems) {

        int beforeSize = orderLog.size();

        assert orderTable >= 0;
        assert menuItems.size() > 0;

        ArrayList<MenuItem> orderItems = new ArrayList<>();
        if (searchOrder(orderId)) {
            return -1; // Order ID already exists
        }
        if (menuItems.size() <= 0) {
            return -2; // Not enough menu items
        }

        MenuItem oneItem;
        //Search if the menu items exists in the catalogue and update the list
        for (Integer menuId : menuItems) {
            oneItem = searchMenuId(menuId);
            if (oneItem == null)
                return -3; // Menu item not found
            orderItems.add(oneItem);
        }
        Order newOrder = new Order(orderId, orderTable);
        OrderDisplay newDisplay = new OrderDisplay(orderId, newOrder.getDate(), orderTable, calculatePrice(orderId), menuItems);

        setChanged();
        notifyObservers(newDisplay);

        orderLog.put(newOrder, orderItems);

        assert newOrder != null;
        assert orderLog.containsKey(newOrder);
        assert orderLog.get(newOrder) != null;
        assert beforeSize + 1 == orderLog.size();
        return 0;
    }

    public boolean searchMenu(int menuId) {
        for (MenuItem menu : restaurantMenu) {
            if (menu.getId() == menuId) {
                return true;
            }
        }
        return false;
    }

    public int addItem(int menuId, String menuName, int menuPrice, ArrayList<Integer> menuItems) {

        int beforeSize = restaurantMenu.size();

        assert isWellFormed();
        assert menuId >= 0;
        assert !menuName.equals("");
        assert menuPrice > 0;

        // Verify if the id exists
        if (searchMenu(menuId)) {
            return -1; // Menu with this id is already in the list
        }
        if (menuItems.size() <= 0) {
            restaurantMenu.add(new BaseProduct(menuId, menuPrice, menuName));
        } else {
            ArrayList<MenuItem> componentItems = new ArrayList<>();
            MenuItem oneItem;
            for (Integer id : menuItems) {
                oneItem = searchMenuId(id);
                if (oneItem == null) {
                    return -2; // Can't create composite menu because the id of the product doesn't exist
                }
                componentItems.add(oneItem);
            }
            CompositeProduct newProduct = new CompositeProduct(menuId, menuName);
            for (MenuItem item : componentItems) {
                newProduct.addComponentItems(item);
            }
            newProduct.increasePrice(menuPrice);
            restaurantMenu.add(newProduct);
        }

        assert isWellFormed();
        assert restaurantMenu.size() == beforeSize + 1;

        return 0;
    }

    public int editItem(int menuId, String menuName, int menuPrice, ArrayList<Integer> menuItems) {

        int beforeSize = restaurantMenu.size();

        assert isWellFormed();
        assert menuId >= 0;
        assert !menuName.equals("");
        assert menuPrice > 0;

        if (!searchMenu(menuId)) {
            return -1; // Menu with this id is not in the list
        }
        MenuItem oneItem = searchMenuId(menuId);
        if (menuName.length() == 0) {
            return -2; // Cannot add product with no name;
        }
        if (oneItem instanceof BaseProduct && menuItems.size() > 0) {
            return -3; // Cannot convert base product to composite product (Try to create a new Product rather than edit one)
        }
        if (oneItem instanceof BaseProduct) {
            ((BaseProduct) oneItem).setBasePrice(menuPrice);
        } else if (oneItem instanceof CompositeProduct) {
            if (menuItems.size() == 0) {
                return -4; // Cannot convert composite product to base product
            }
            ArrayList<MenuItem> componentItems = new ArrayList<>();
            MenuItem auxItem;
            for (Integer id : menuItems) {
                auxItem = searchMenuId(id);
                if (auxItem == null) {
                    return -5; // Can't edit composite menu because the id of the product doesn't exist
                }
                componentItems.add(auxItem);
            }
            ((CompositeProduct) oneItem).setCompositePrice(0);
            ((CompositeProduct) oneItem).setComponentItems(new ArrayList<>());
            for (MenuItem item : componentItems) {
                ((CompositeProduct) oneItem).addComponentItems(item);
            }
            ((CompositeProduct) oneItem).increasePrice(menuPrice);
        } else {
            return -6; // What kind of error is this?
        }
        oneItem.setName(menuName);

        assert isWellFormed();
        assert restaurantMenu.size() == beforeSize + 1;

        return 0;
    }

    public int deleteItem(int menuId) {
        int beforeSize = restaurantMenu.size();

        assert isWellFormed();
        assert menuId >= 0;

        for (MenuItem item : restaurantMenu) {
            if (menuId == item.getId()) {
                restaurantMenu.remove(item);

                assert isWellFormed();
                assert restaurantMenu.size() == beforeSize - 1;

                return 0;
            }
        }

        return -1; // Menu with this id is not in the list
    }

    public int calculatePrice(int orderId) {

        assert orderId >= 0;

        for (Map.Entry<Order, ArrayList<MenuItem>> entry : orderLog.entrySet()) {
            Order key = entry.getKey();
            ArrayList<MenuItem> value = entry.getValue();
            if (key.getOrderId() == orderId) {
                int sum = 0;
                for (MenuItem menu : value) {
                    sum += menu.computePrice();
                }
                assert sum >= 0;
                return sum;
            }
        }
        return -1;
    }

    public String generateBill(int orderId) {

        assert orderId >= 0;

        Order order;
        ArrayList<MenuItem> items;
        int sum = 0;
        for (Map.Entry<Order, ArrayList<MenuItem>> entry : orderLog.entrySet()) {
            Order key = entry.getKey();
            ArrayList<MenuItem> value = entry.getValue();
            if (key.getOrderId() == orderId) {
                order = key;
                items = value;
                for (MenuItem menu : value) {
                    sum += menu.computePrice();
                }
                return String.format("%s\n\nMenu Items:\n%s\n\nTotal price= %d", order.toString(), items, sum);
            }
        }
        return "";
    }

    public boolean isWellFormed() {
        int nrItems = 0;
        for (MenuItem item : restaurantMenu) {
            if (item.getId() >= 0 && !item.getName().equals("") && item.computePrice() > 0) {
                nrItems++;
            }
        }
        return nrItems == restaurantMenu.size();
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "restaurantMenu=" + restaurantMenu +
                '}';
    }
}
