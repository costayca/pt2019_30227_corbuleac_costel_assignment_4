package business;

import java.util.Date;
import java.util.Objects;

public class Order {
    private int orderId;
    private Date date;
    private int table;

    public Order(int orderId, int table) {
        this.orderId = orderId;
        this.date = new Date(System.currentTimeMillis());
        this.table = table;
    }

    public int getOrderId() {
        return orderId;
    }

    public Date getDate() {
        return date;
    }

    public int getTable() {
        return table;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Order order = (Order) o;
        return orderId == order.orderId &&
                table == order.table &&
                Objects.equals(date, order.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(orderId, date, table);
    }

    @Override
    public String toString() {
        return String.format("Order:\norderId= %d\ndate= %s\ntable= %d\n", orderId, date, table);
    }
}
