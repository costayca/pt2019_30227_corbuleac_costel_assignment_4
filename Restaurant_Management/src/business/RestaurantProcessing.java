package business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public interface RestaurantProcessing {
    public HashMap<Order, ArrayList<MenuItem>> getOrderLog();

    public Set<MenuItem> getRestaurantMenu();

    public Set<Order> getOrders();

    /**
     * @pre menuId >= 0
     * @pre !menuName.equals("")
     * @pre menuPrice >= 0
     * @post Restaurant.restaurantMenu.size() == Restaurant.restaurantMenu.size()@pre + 1
     * @invariant isWellFormed()
     */
    public int addItem(int menuId, String menuName, int menuPrice, ArrayList<Integer> menuItems);

    /**
     * @pre menuId >= 0
     * @pre !menuName.equals("")
     * @pre menuPrice >= 0
     * @post Restaurant.restaurantMenu.size() == Restaurant.restaurantMenu.size()@pre + 1
     * @invariant isWellFormed()
     */
    public int editItem(int menuId, String menuName, int menuPrice, ArrayList<Integer> menuItems);

    /**
     * @pre menuId >= 0
     * @post Restaurant.restaurantMenu.size() == Restaurant.restaurantMenu.size()@pre - 1
     * @invariant isWellFormed()
     */
    public int deleteItem(int menuId);

    /**
     * @pre orderTable >= 0
     * @pre menuItems.size() > 0
     * @post @return != null
     * @post Restaurant.orderLog.containsKey(@ return) == true
     * @post Restaurant.orderLog.get(@ return) != null
     * @post Restaurant.orderLog.size() = Restaurant.orderLog.size()@pre + 1
     */
    public int addOrder(int orderId, int orderTable, ArrayList<Integer> menuItems);


    /**
     * @return >= 0
     * @pre orderId>=0
     */
    public int calculatePrice(int orderId);

    /**
     * @pre orderId >=0
     */
    public String generateBill(int orderId);
}
