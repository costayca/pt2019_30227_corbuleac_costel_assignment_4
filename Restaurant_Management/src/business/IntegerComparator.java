package business;

import java.io.Serializable;
import java.util.Comparator;

public class IntegerComparator implements Comparator<MenuItem> {
    @Override
    public int compare(MenuItem o1, MenuItem o2) {
        return Integer.compare(o1.getId(),o2.getId());
    }
}
